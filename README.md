# NY Times Reviews

## Introduction

Android app fetching movie reviews from NY Times API.
Compatible from Android API 21.

**Master branch** consumes a limited number of items from Movie reviews picks API.

**Paging branch** is using paging library for both network and database in order to provide an endless list of reviews.  
I have never implemented pagination before, so I practiced with that [Codelab](https://codelabs.developers.google.com/codelabs/android-paging/index.html) from Google.  
It uses Paging v3 which is compatible with Coroutines but still in alpha version (also need Room latest alpha version).

**Note**: If you switch to paging branch, please uninstall app first because I didn't handle the database migration. Otherwise app will crash at launch.

### Features implemented
* NY Times API consuming and parsing JSON response
* Caching data into Room database
* Showing a list of reviews in a vertical orientation
* Showing a detail view linked to the review list
* Opening link to NY Times website
* Review list items are sorted by date (database query)
* Gesture navigation on detail screen (I used a class found on Stackoverflow to detech right and left swipe)
    * Works on device Nexus 5X (API 27)
    * Works on simulator Nexus 4 (API 21)
    * Works on simulator Nexus 5X (API 2) and Pixel 2 XL (API 28) only when some breakpoints are set
* Endless list (Paging branch)
* Unit test for mapper

### Improvements (TO DO)
* Rest API error handling
* Having UseCase to improve separation of concerns
* Glide error handling and placeholder

## Implementation

Project is following MVVM pattern.

### Data layer

Retrofit is used as a REST client in order to consume NY Times API. I'm using an interceptor to add api key to the request parameters.  
Once data is parsed, I store the reviews in Room database as cache. Queries are executed in the ReviewDao class.  
The reviews retrieval is done via an observable query implemented using Flow. Coroutines is used to manage the threads.

### Domain layer

It would be nice to add UseCases to improve separation of concerns even if it adds a lot of boilerplate.  
I'm using a mapper to map API review object to the local review object. I implemented a unit test for that mapper to help avoiding regression if code changes.

### Presentation layer

The ViewModels are containing data as LiveData object.
Activity and fragments are observing ViewModel properties in order to bind data to the views.
Navigation between screens, passing data and transitions are handled through Navigation library.

## Dependencies
* Retrofit
* Room
* Coroutines
* Koin
* Navigation
* ConstraintLayout
* ViewModel
* Lifecycle
* Glide
* Paging (paging branch)
