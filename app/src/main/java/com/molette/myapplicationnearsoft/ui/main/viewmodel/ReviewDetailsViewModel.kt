package com.molette.myapplicationnearsoft.ui.main.viewmodel

import android.content.Intent
import android.net.Uri
import androidx.lifecycle.*
import com.molette.myapplicationnearsoft.data.repository.ReviewRepository
import com.molette.myapplicationnearsoft.domain.models.local.ReviewDb

class ReviewDetailsViewModel(private val reviewRepository: ReviewRepository): ViewModel() {

    val reviewId: MutableLiveData<String> = MutableLiveData()

    val review: LiveData<ReviewDb> = reviewId.switchMap {
        reviewRepository.getReview(it).asLiveData()
    }
}