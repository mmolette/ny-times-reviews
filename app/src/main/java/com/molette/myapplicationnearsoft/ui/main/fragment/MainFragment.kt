package com.molette.myapplicationnearsoft.ui.main.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.molette.myapplicationnearsoft.R
import com.molette.myapplicationnearsoft.ui.main.adapter.ReviewAdapter
import com.molette.myapplicationnearsoft.ui.main.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.main_fragment.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {

    private val mainViewModel by viewModel(MainViewModel::class)
    private lateinit var adapter: ReviewAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.main_fragment, container, false)
        adapter = ReviewAdapter(findNavController())
        view.reviewsRecyclerview.adapter = adapter

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mainViewModel.reviews.observe(viewLifecycleOwner, Observer {
            adapter.data = it
        })
    }
}