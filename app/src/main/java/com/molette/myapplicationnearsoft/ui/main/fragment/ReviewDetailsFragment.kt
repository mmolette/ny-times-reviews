package com.molette.myapplicationnearsoft.ui.main.fragment

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.molette.myapplicationnearsoft.R
import com.molette.myapplicationnearsoft.databinding.FragmentReviewDetailsBinding
import com.molette.myapplicationnearsoft.domain.models.local.ReviewDb
import com.molette.myapplicationnearsoft.ui.main.viewmodel.ReviewDetailsViewModel
import com.molette.myapplicationnearsoft.utils.OnSwipeTouchListener
import org.koin.androidx.viewmodel.ext.android.viewModel


class ReviewDetailsFragment : Fragment() {

    private val args: ReviewDetailsFragmentArgs by navArgs()
    private val detailsViewModel by viewModel(ReviewDetailsViewModel::class)
    private lateinit var binding: FragmentReviewDetailsBinding
    private lateinit var hanlder: ReviewDetailsHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        hanlder = ReviewDetailsHandler(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate<FragmentReviewDetailsBinding>(inflater, R.layout.fragment_review_details, container, false)
        binding.handler = hanlder
        binding.reviewDetailsContainer.setOnTouchListener(object: OnSwipeTouchListener(requireContext()){
            override fun onSwipeRight() {
                super.onSwipeRight()
                requireActivity().onBackPressed()
            }
        })
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        detailsViewModel.reviewId.value = args.reviewTitle
        detailsViewModel.review.observe(viewLifecycleOwner, Observer {
            binding.review = it
            Glide.with(requireContext())
                .load(it.thumbnail)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(binding.reviewDetailsImage)
        })
    }


}

class ReviewDetailsHandler(val context: Context){

    fun onLinkToReviewClick(review: ReviewDb){
        val intent = Intent(Intent.ACTION_VIEW)
        intent.setData(Uri.parse(review.link))
        context.startActivity(intent)
    }
}