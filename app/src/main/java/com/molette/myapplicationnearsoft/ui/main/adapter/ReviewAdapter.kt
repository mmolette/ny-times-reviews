package com.molette.myapplicationnearsoft.ui.main.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.molette.myapplicationnearsoft.R
import com.molette.myapplicationnearsoft.databinding.ReviewCellBinding
import com.molette.myapplicationnearsoft.domain.models.local.ReviewDb
import com.molette.myapplicationnearsoft.ui.main.fragment.MainFragmentDirections

class ReviewAdapter(private val navController: NavController): RecyclerView.Adapter<ReviewViewHolder>() {

    var data: List<ReviewDb> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewViewHolder {

        val inflater: LayoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ReviewCellBinding>(inflater, R.layout.review_cell, parent,  false)

        return ReviewViewHolder(binding, navController, parent.context)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {

        val review = data[position]
        holder.bind(review)
    }
}

class ReviewViewHolder(val binding: ReviewCellBinding, val navController: NavController, val context: Context): RecyclerView.ViewHolder(binding.root){

    fun bind(review: ReviewDb){
        binding.review = review
        Glide.with(context)
            .load(review.thumbnail)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(binding.reviewThumbnail)

        binding.root.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?){
                val navDirections = MainFragmentDirections.actionMainFragmentToReviewDetailsFragment(reviewTitle = review.title)
                navController.navigate(navDirections)
            }
        })
    }
}