package com.molette.myapplicationnearsoft.ui.main.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.molette.myapplicationnearsoft.data.repository.ReviewRepository
import com.molette.myapplicationnearsoft.domain.models.local.ReviewDb
import kotlinx.coroutines.launch

class MainViewModel(val reviewRepository: ReviewRepository) : ViewModel() {

    val reviews: LiveData<List<ReviewDb>> = reviewRepository.reviews.asLiveData()

    init {
        getReviews()
    }

    private fun getReviews(){
        viewModelScope.launch {
            reviewRepository.getReviewsRemote()
        }
    }
}