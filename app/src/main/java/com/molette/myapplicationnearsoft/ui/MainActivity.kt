package com.molette.myapplicationnearsoft.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.molette.myapplicationnearsoft.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
    }
}