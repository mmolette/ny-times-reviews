package com.molette.myapplicationnearsoft.di

import com.molette.myapplicationnearsoft.data.api.ApiService
import com.molette.myapplicationnearsoft.data.db.AppDatabase
import com.molette.myapplicationnearsoft.data.repository.ReviewRepository
import com.molette.myapplicationnearsoft.data.repository.implementation.ReviewRepositoryImpl
import com.molette.myapplicationnearsoft.ui.main.viewmodel.MainViewModel
import com.molette.myapplicationnearsoft.ui.main.viewmodel.ReviewDetailsViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

var appModule = module {
    // API
    factory { ApiService.createReviewAPI()  }

    // Database
    single { AppDatabase.buildDb(context = androidContext()) }

    // Repositories
    factory<ReviewRepository> { ReviewRepositoryImpl(get(), get()) }

    // Viewmodels
    viewModel { MainViewModel(get()) }
    viewModel { ReviewDetailsViewModel(get()) }
}