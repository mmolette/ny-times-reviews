package com.molette.myapplicationnearsoft.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.molette.myapplicationnearsoft.data.db.dao.ReviewDao
import com.molette.myapplicationnearsoft.domain.models.local.ReviewDb

@Database(entities = [ReviewDb::class],
    version = 1, exportSchema = false)
abstract class AppDatabase(): RoomDatabase() {

    abstract val reviewDao: ReviewDao

    companion object {
        const val DATABASE_NAME = "reviews_db"

        fun buildDb(context: Context): AppDatabase{
            return Room.databaseBuilder(context,
                AppDatabase::class.java,
                DATABASE_NAME)
                .addCallback(object : Callback(){
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                    }
                }).build()
        }
    }

}