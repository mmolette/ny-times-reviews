package com.molette.myapplicationnearsoft.data.api

import com.molette.myapplicationnearsoft.data.api.models.remote.Response
import com.molette.myapplicationnearsoft.data.api.models.remote.ReviewRemote
import retrofit2.http.GET

interface ReviewAPI {

    @GET("reviews/picks.json")
    suspend fun getReviews(): Response<ReviewRemote>
}