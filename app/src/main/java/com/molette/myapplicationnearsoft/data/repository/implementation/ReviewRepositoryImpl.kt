package com.molette.myapplicationnearsoft.data.repository.implementation

import android.util.Log
import com.molette.myapplicationnearsoft.data.api.ReviewAPI
import com.molette.myapplicationnearsoft.data.db.AppDatabase
import com.molette.myapplicationnearsoft.data.repository.ReviewRepository
import com.molette.myapplicationnearsoft.domain.mappers.ReviewMapper
import com.molette.myapplicationnearsoft.domain.models.local.ReviewDb
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import java.lang.Exception

class ReviewRepositoryImpl(private val remoteDataSource: ReviewAPI, private val localDataSource: AppDatabase):
    ReviewRepository {

    override val reviews: Flow<List<ReviewDb>> = localDataSource.reviewDao.getAll()
    private val mapper = ReviewMapper()

    override suspend fun getReviewsRemote() {
        withContext(Dispatchers.IO){

            try {
                val reviews = remoteDataSource.getReviews().results

                val reviewsDb: List<ReviewDb> = reviews.map {
                    mapper.mapFromRemote(it)
                }
                val inserted = localDataSource.reviewDao.insertAll(reviewsDb)
            }catch(e: Exception){
                Log.e("getReviewsRemote", Log.getStackTraceString(e))
            }
        }
    }

    override fun getReview(title: String): Flow<ReviewDb> {
        return localDataSource.reviewDao.getReview(title)
    }
}