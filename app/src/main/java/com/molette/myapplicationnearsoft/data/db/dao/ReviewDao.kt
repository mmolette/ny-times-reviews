package com.molette.myapplicationnearsoft.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.molette.myapplicationnearsoft.domain.models.local.ReviewDb
import kotlinx.coroutines.flow.Flow

@Dao
interface ReviewDao: BaseDao<ReviewDb> {

    @Query("SELECT * FROM reviews ORDER BY published_date DESC")
    fun getAll(): Flow<List<ReviewDb>>

    @Query("SELECT * FROM reviews WHERE title = :title")
    fun getReview(title: String): Flow<ReviewDb>
}