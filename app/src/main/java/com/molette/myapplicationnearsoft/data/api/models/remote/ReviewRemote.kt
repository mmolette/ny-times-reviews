package com.molette.myapplicationnearsoft.data.api.models.remote

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ReviewRemote(
    @SerialName("display_title")
    val displayTitle: String,
    @SerialName("publication_date")
    val date: String,
    val headline: String,
    val multimedia: MultimediaRemote?,
    val link: LinkRemote?,
    val byline: String,
    @SerialName("summary_short")
    val summary: String
) {
}

/*
"display_title": "The Black Godfather",
"mpaa_rating": "",
"critics_pick": 0,
"byline": "BEN KENIGSBERG",
"headline": "‘The Black Godfather’ Review: The Music Executive Who Made It All Happen",
"summary_short": "Reginald Hudlin’s documentary about Clarence Avant includes many golden anecdotes.",
"publication_date": "2019-06-06",
"opening_date": "2019-06-07",
"date_updated": "2019-06-14 16:44:01",
"link": {
    "type": "article",
    "url": "http://www.nytimes.com/2019/06/06/movies/the-black-godfather-review.html",
    "suggested_link_text": "Read the New York Times Review of The Black Godfather"
},
"multimedia": {
    "type": "mediumThreeByTwo210",
    "src": "https://static01.nyt.com/images/2019/06/05/arts/blackgodfather1/blackgodfather1-mediumThreeByTwo210.jpg",
    "width": 210,
    "height": 140
}
*/
