package com.molette.myapplicationnearsoft.data.api.models.remote

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class LinkRemote(
    val type: String,
    val url: String,
    @SerialName("suggested_link_text")
    val description: String
){

}