package com.molette.myapplicationnearsoft.data.api

import com.molette.myapplicationnearsoft.BuildConfig
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import okhttp3.MediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

class ApiService {

    companion object {

        private val contentType = MediaType.get("application/json")
        private val apiInterceptor: ApiKeyInterceptor = ApiKeyInterceptor()

        private val retrofit = Retrofit.Builder()
            .client(createOkHttpClient())
            .baseUrl(BuildConfig.NYT_BASE)
            .addConverterFactory(
                Json(configuration = JsonConfiguration(
                    useArrayPolymorphism = true,
                    encodeDefaults = false, ignoreUnknownKeys = true
                )
                ).asConverterFactory(contentType))
            .build()

        fun createReviewAPI(): ReviewAPI {

            return retrofit.create(ReviewAPI::class.java)
        }

        private fun createOkHttpClient(): OkHttpClient {

            val builder = OkHttpClient.Builder()

            // Interceptor to add api key
            builder.addInterceptor(apiInterceptor)
            builder
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)

            builder.retryOnConnectionFailure(true)

            return builder.build()
        }
    }
}