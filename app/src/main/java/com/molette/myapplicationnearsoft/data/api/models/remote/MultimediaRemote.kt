package com.molette.myapplicationnearsoft.data.api.models.remote

import kotlinx.serialization.Serializable

@Serializable
data class MultimediaRemote(
    val type: String,
    val src: String,
    val width: Int,
    val height: Int
) {

}