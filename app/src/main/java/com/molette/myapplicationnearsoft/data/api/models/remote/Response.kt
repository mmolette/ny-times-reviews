package com.molette.myapplicationnearsoft.data.api.models.remote

import kotlinx.serialization.Serializable

@Serializable
class Response<T>(
    val status: String,
    val results: List<T> = listOf()
) {

}