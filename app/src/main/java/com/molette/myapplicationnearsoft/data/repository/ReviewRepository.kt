package com.molette.myapplicationnearsoft.data.repository

import com.molette.myapplicationnearsoft.domain.models.local.ReviewDb
import kotlinx.coroutines.flow.Flow

interface ReviewRepository {

    val reviews: Flow<List<ReviewDb>>

    suspend fun getReviewsRemote()

    fun getReview(title: String): Flow<ReviewDb>
}