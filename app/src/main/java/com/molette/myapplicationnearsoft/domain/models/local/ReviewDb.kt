package com.molette.myapplicationnearsoft.domain.models.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "reviews")
data class ReviewDb(
    @PrimaryKey
    val title: String,
    @ColumnInfo(name = "published_date")
    val publishedDate: String,
    val headline: String,
    val thumbnail: String?,
    val author: String,
    val summary: String,
    val link: String?
) {

}