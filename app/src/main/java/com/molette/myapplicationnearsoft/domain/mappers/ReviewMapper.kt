package com.molette.myapplicationnearsoft.domain.mappers

import com.molette.myapplicationnearsoft.domain.models.local.ReviewDb
import com.molette.myapplicationnearsoft.data.api.models.remote.ReviewRemote

class ReviewMapper() : ModelMapper<ReviewRemote, ReviewDb> {

    override fun mapFromRemote(model: ReviewRemote): ReviewDb {
        return ReviewDb(
            title = model.displayTitle,
            publishedDate = model.date,
            headline = model.headline,
            thumbnail = model.multimedia?.src,
            author = model.byline,
            summary = model.summary,
            link = model.link?.url
        )
    }
}