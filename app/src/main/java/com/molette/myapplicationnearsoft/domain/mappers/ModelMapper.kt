package com.molette.myapplicationnearsoft.domain.mappers

interface ModelMapper<in M, out E> {

    fun mapFromRemote(model: M): E
}