package com.molette.myapplicationnearsoft

import com.molette.myapplicationnearsoft.data.api.models.remote.LinkRemote
import com.molette.myapplicationnearsoft.data.api.models.remote.MultimediaRemote
import com.molette.myapplicationnearsoft.data.api.models.remote.ReviewRemote
import com.molette.myapplicationnearsoft.domain.mappers.ReviewMapper
import org.hamcrest.MatcherAssert
import org.hamcrest.core.Is
import org.hamcrest.core.IsEqual
import org.junit.Test
import org.junit.Assert.*

class ReviewMapperUnitTest {

    val remote = ReviewRemote(
        displayTitle = "Title test",
        byline = "Mathieu Molette",
        headline = "Headline test",
        link = LinkRemote(type = "url", url = "http://www.test.com", description = "Link description"),
        date = "2019-06-06",
        summary = "Summary test",
        multimedia = MultimediaRemote(type = "image", src = "image.jpg", width = 40, height = 40)
    )

    @Test
    fun reviewMapping_isCorrect(){

        val local = ReviewMapper().mapFromRemote(remote)

        assertThat(local.title, IsEqual(remote.displayTitle))
        assertThat(local.author, IsEqual(remote.byline))
        assertThat(local.headline, IsEqual(remote.headline))
        assertThat(local.link, IsEqual(remote.link.url))
        assertThat(local.publishedDate, IsEqual(remote.date))
        assertThat(local.summary, IsEqual(remote.summary))
        assertThat(local.thumbnail, IsEqual(remote.multimedia.src))
    }
}